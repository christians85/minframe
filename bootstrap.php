<?php

use Minframe\Core\Router;
use Minframe\Core\Template;
use Minframe\Core\InputMerger;
use Minframe\Core\Utils;
use Minframe\Core;

// Path-Constants must be initialized before everything else
define("ROOT_DIR", realpath(__DIR__));
require_once ROOT_DIR . '/config/paths.php';

/*
 * Loads Vendor:
 *
 * This project uses PSR-4-Autoloading of Composer.
 *  
 * All Classes inside of src/ need to live in namespace Minframe.
 * 
 * All Sub-Classes, e.g. of "Core" need to be placed inside of
 * src/Core and have to live in namespace Mainframe/Core.
 *  
 * Namespaces and their representing folders are case sensitive!
 */
require_once ROOT_DIR . '/vendor/autoload.php';


// Fix Paths for Routing while using XAMPP
$basePath = "/";
$realPath = Utils::serv('PHP_SELF');
$positionBeforeIndex = strpos($realPath, '/index.php');
// If there was given a position > 0: Erase everything before /index.php
if($positionBeforeIndex && $positionBeforeIndex > 0) {
    $basePath = substr($realPath, 0, $positionBeforeIndex) . "/";
    $_SERVER['REQUEST_URI'] = str_replace(['/index.php', $basePath], "/", $_SERVER['REQUEST_URI']);
}
$basePath = str_replace("public/", "", $basePath);
// This constant must not be used to help the website find css, js, images etc.
define("PUB_PATH", "http://" . Utils::serv('HTTP_HOST') . $basePath);
// @todo: Make this Compatible to HTTPS

// GET and POST Fields
// $result = InputMerger::get();

// // DB Handler - uncomment after Setting dbconstants
// $db = Core\getDB();
