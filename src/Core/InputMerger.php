<?php

namespace Minframe\Core;

/* 
 * This has the purpose to resolte all data from
 * GET and POST into an array
 */
class InputMerger {

    /**
     * Returns all Values of GET and POST in one array
     * after sanitizing it.
     */
    public static function get() {
        if(sizeof($_GET) < 1 && sizeof($_POST) < 1) {
            return array();
        }
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        $post = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        return array_merge($get, $post);
    }
}