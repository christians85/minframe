<?php


namespace Minframe\Core;
use Minframe\Core\Utils;

require_once ROOT_DIR . "/config/paths.php";

class Template {


    /**
     * Escapes all HTML-Tags so that nothing can be executed by Cross Site Scritping
     * @param $str String you want to output
     * @return string The sane output-text
     */
    function escape($str = null):string {
        return ($str ? htmlspecialchars($str, ENT_QUOTES, 'UTF-8') : "");
    }

    /**
     * Simple function to place templates inside the website.
     * @param string $template      Template Name without path. If template is in subfolder, use / to seperate.
     * @param array $data           Array with data to place in Template.
     */
    public static function render(string $template, array $data) {

        // Removes .php Postfix is alrady passed to function
        str_replace(".php", "", $template);
        // Takes the Template-String and resolves it into a usable path:
        $path = TEMPLATE_DIR . DS . $template . ".php";
        
        // Checks if template path even exists.
        if(!is_file($path)) {
            trigger_error("Das Template unter $path konnte nicht geladen werden oder existiert nicht.");
            return null;
        }

        // extract everything from array
        extract($data, EXTR_SKIP);

        // takes all following into the buffer and returns it
        ob_start();
        include($path);    // here the template is loaded, all data is placed into it
        return trim(ob_flush());
    }


    /**
     * Calls the place function for every node in array
     * @param string $template      Template Name without path. If template is in subfolder, use / to seperate
     * @param array $data           2-dimensional array with data to place in template
     */
    public static function renderLoop(string $template, array $data) {
        foreach ($data as $node) {
            self::render($template, $node);
        }
    }
}
