<?php

namespace Minframe\Core;
use Minframe\Core\Router;
use Minframe\Core\Template;

/*
 * If you want just to add /thepage:
 *      Browserline: www.your-domain.com/thepage, REGEX: '/^thepage$/'
 * 
 * If you want just to add /thepage/subpage:
 *      Browserline: www.your-domain.com/thepage, REGEX: '/^thepage\/subpage$/'
 * 
 * If you want to add a page with a variable, e.g. a page:
 *      Browserline: www.your-domain.com/article/5 (to view the article with e.g. ID 5):
 *      REGEX: '/^article\/(?P<id>\d+)$/'
 *             This adds a Variable id with decimal value
 * 
 * The examples are below, commented out.
 */


Router::add(    // /index -> startseite
    '/^index(\/?)$/',
    function() {
        $title = "Wilkommen";
        $text = "<p>Die Demo von meinem minimalen Framework \"Minframe\" wurde erfolgreich geladen, dies ist die Startseite!</p>";
        Template::render('basictext', [
            'title' => $title, 
            'text' => $text
        ]);
    }
);

Router::add(    // /index -> startseite
    '/^todo(\/?)$/',
    function() {
        $title = "Hier wird noch gebaut.";
        $text = "<p>So könnte eine Seite aussehen, die noch nicht fertig ist.</p>";
        Template::render('basictext', [
            'title' => $title, 
            'text' => $text
        ]);
    }
);



Router::add(
    '/^article\/(?P<id>\d+)(\/?)$/',
    function(int $x) {
        echo "You can now read Article with id " . $x;
    }
);


Router::add(
    '/^(?P<perpage>\d+)(\/?)$/',
    function(int $x) {
        echo "<p>Sie sehen " . $x . " Artikel:</p>\n";
        echo "<ul>\n";

        for ($i = 0; $i < $x; $i++) {
            echo "\t<li>Artikel Nr. " . $i . "</li>\n";
        }

        echo "</ul>\n";
    }
);


Router::add(
    '/^(?P<perpage>\d+)\/(?P<offset>\d+)(\/?)$/',
    function(int $perpage, $offset) {
        echo "<p>Sie sehen " . $perpage . " Artikel, beginnend mit " . $offset . ":</p>\n";
        echo "<ul>\n";

        for ($i = $offset; $i < $offset + $perpage ; $i++) {
            echo "\t<li>Artikel Nr. " . $i . "</li>\n";
        }

        echo "</ul>\n";
    }
);

// Testpage to validate that a internal route can only be accessed internally
Router::add(        // / -> startseite
    '/testen(\/?)$/',
    function() {
        echo "Testseite!";
    },
    true
);

// // The Examples
// Router::add(
//     '/^thepage$/',
//     function() {
//         echo "Willkommen auf der THEPAGE!";
//     },
// );

// Router::add(
//     '/^thepage\/subpage$/',
//     function() {
//         echo "Willkommen auf der THEPAGE / SUBPAGE!";
//     }
// );