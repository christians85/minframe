<?php

namespace Minframe\Core;

use Minframe\Core\Utils;

require_once __DIR__ . "../routes.php";

/*
 * This class provides a routing capability to make URLs
 * for the browser more beutiful and make including of
 * internal used code parts easier.
 * 
 * I created this with the help of the following article:
 *       http://www.stephan-romhart.de/artikel/performantes-url-routing-php-regex
 */
class Router {

    private static $routes = [];


    /**
     * Adds a route into the routing-array.
     * @param string $pattern Pattern of the route.
     * @param string $ctrl The path to the controller.
     */
    public static function add(string $pattern, callable $func, bool $internal = false) {
        // Pushes the new route into the array.
        array_push(static::$routes, array(
            'pattern' => $pattern,
            'func' => $func,
            'internal' => $internal)
        );
    }

    /**
     * Resolves a Route, calls the callable if the route exists
     * @param $path the path of the route
     * @todo a sane use for request method has to be implemented
     * @todo multiple routes can be accesses the same time
     */
    public static function route($path = null) {
        
        

        $match = false;
        $args = array();

        // Helps to resolve if a route was called internally
        // or via browser adress line.
        $browserpath = str_replace("route=", "", Utils::serv('QUERY_STRING'));

        // If no path is given use Server-Variable to resolve Route
        if(!$path) $path = $browserpath; 

        // Removes unused prefix from Path.
        $path = str_replace("route=", "", $path);

        // If there is no route given -> Display main Route
        if($path == "") self::route("index");

        // Path is required
        if(!$path) return static::$routes;

        // If patch contains ".." -> escape from method.
        if(strpos($path, '..') !== false) return;

        /*
         * Iterates over all routes and checks if there is any
         * route that matches the pattern.
         */
        foreach (static::$routes as $route) {
            if ($match = preg_match($route['pattern'], $path, $matches)) {
                $args = array_merge($args, $matches);
                break;
            }
        }
        
        /*
         * Removes duplicates from the arguments which will be
         * passed to the func so that it can be called correctly.
         */
        array_shift($args);
        foreach($args as $key => $arg) {   
            if(!is_int($key)) {
                unset($args[$key]);
            }
        }

        // If there was no match -> There is no route defined.
        if (!$match) return Event::event(404);

        // If there is no callable -> Route cannot be executed!
        if (!is_callable($route['func'])) exit ("Die Route " . $path . " existiert, ist aber nicht spezifiziert.");

        // If the Route was internal but tried to call from browser adress line:
        if(
               $browserpath === $path       // Browserpath divide from each other
            && $route['internal']           // Route is internal
            && Utils::serv('REQUEST_URI')   // a URI is given
            
        ) {
            return Event::event(404);       // Fake that the Route is not existend
        }

        // If nothing happened so far -> The Route is okay -> Execute it!
        $callback = $route['func'](...$args);
        return $callback;
    }
}