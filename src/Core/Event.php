<?php

namespace Minframe\Core;

require_once "events.php";

/*
 * This is a simple Event System.
 * You can register a argument by method addEvent that will await
 * 2 - 3 parameters, containing name, a callable function and a optional priority.
 * 
 * After creation of event you can trigger it by the event Method given
 * the name of the event you want to trigger and a array of params.
 * 
 * This allows to register functions to an event and call it whenever and
 * wherever you want.
 */

 class Event {

    private static $events;
    
    /**
     * Registers an Event to the eventsystem or adds a function
     * to a already existing event.
     * 
     * @param string $name Name of the event you want to create or extend
     * @param callable $func Function to run when event is triggered
     * @param int $prior Priority of the function
     */
    public static function addEvent(
        string $name, 
        int $prior = 0,
        callable  $func 
        )
    {

        // Every Event needs a name.
        if(!$name) return false;

        // Every Event needs a callable.
        if(!is_callable($func)) return false;

        // Register the Event
        static::$events[$name][$prior] = array(
            // 'belong' => $name,
            // 'prior' => $prior,
            'func' => $func
        );

        /*
         * There can be multiple functions attached to one event.
         * To ensure that the function with highest priority always is 
         * called first, sort the array when new event is registered.
         */
        krsort(static::$events[$name], SORT_NUMERIC);
    }

    /**
     * Triggers the event based on it's name. You can pass a array of arguments
     * that will be applied on the callable.
     * @param string $name Name of the event to trigger.
     * @param array $args Array of arguments for the functions
     * @param bool $directdisplay True if functions print something and you want to do it right at calling.
     */
    public static function event(string $name, array $args = [], bool $directdisplay = true) {
        
        // Ensures that results of function calls can be used after.
        // Only if directdisplay is false.
        if (!$directdisplay) {
            ob_start();
        }

        // Iterates over all events and calls functions.
        foreach(static::$events[$name] as $event) {
            // Getting the values of the given data
            $args = array_values($args);
            
            // // There is a faster and better way of calling the function by using argument unpacking.
            // // -> Source: https://www.php.net/manual/de/function.call-user-func-array.php#117655
            // echo call_user_func_array($event['func'], $args);
            
            if ($directdisplay) {
                echo $event['func'](...$args);
            } else {
                $event['func'](...$args);
            }
        }

        if (!$directdisplay) {
            return ob_get_clean();
        }
    }

    // TODO: Method that creates and triggers event with one call.



    public static function debugPrintEventsByName($name) {
        echo "<li>Functions triggered with Event \"" . $name . "\"<ul>\n";
        foreach (static::$events[$name] as $prior => $func) {
            echo "<li>Priority: " . $prior . "</li>\n";
        }
        echo "</ul></li>\n";
    }

    public static function debugPrintEvents() {
        echo "<ul>\n";
            foreach(static::$events as $name => $event) {
                self::debugPrintEventsByName($name);
            }
        echo "</ul>\n";
    }


 }