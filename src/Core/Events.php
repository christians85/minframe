<?php

namespace Minframe\Core;
use Minframe\Core\Event as Event;

define('404', "err.404");
define('403', "err.403");

Event::addEvent(404, 10, function() {
    echo "\n<h3 class=\"error\">404: Not Found</h3>\n<p class=\"error\">\n\tDie gewünschte Seite existiert leider nicht.\n</p>\n";
});

Event::addEvent(403, 10, function() {
    echo "\n<h3 class=\"error\">403: Forbidden</h3>\n<p class=\"error\">\n\tSie haben leider keine Berechtigung hierzu: Bitte loggen Sie sich ein!.\n</p>\n";
});