<?php

namespace Minframe\Core;

class Utils {

    /**
     * Wrapps the print_r output of an array and makes it easily usable
     * @param array $array Array to display
     */
    public static function printArray(array $array) {
        echo "<pre>\n";
        echo print_r($array);
        echo "\n</pre>\n";
    }


    /**
     * Helper to return a Server-Variable
     * @param $var The Variable you want to use 
     */
    public static function serv($var) {
        return !isset($_SERVER[$var]) ? null : $_SERVER[$var];
    }


    /**
     * Returns if Request is POST
     * @return bool true if post, false if not
     */
    public static function isPost():bool {
        return self::serv('REQUEST_METHOD') === "POST";
    }

    
    /**
     * Returns if Request is GET
     * @return bool true if get, false if not
     */
    public static function isGet():bool {
        return self::serv('REQUEST_METHOD') === "GET";
    }
}