<?php

namespace Minframe\Core;
use PDO;

$db = require_once(ROOT_DIR . "/config/dbconstants.php");

/**
 * Returns the single PDO-Instance
 * @return PDO PDO-Connection
 */
function getDB() {
    // only one instance of PDO-handler
    static $handler;

    // if PDO already was initialized, just return existing db Handler
    if($handler instanceof PDO) return $handler;

    // if there was no instance before, create one and return it
    $handler = new PDO(_DB_DSN_, _DB_USER_, _DB_PWD_);
    return $handler;
}