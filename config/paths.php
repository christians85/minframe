<?php

define ("DS", DIRECTORY_SEPARATOR);

define("CONFIG_DIR", realpath(__DIR__));
define("TEMPLATE_DIR", realpath(ROOT_DIR . "/templates"));
define("PUBLIC_DIR", realpath(ROOT_DIR . "/public"));