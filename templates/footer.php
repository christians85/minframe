      <footer class="pt-4 my-md-5 pt-md-5 border-top">
        <b>Please note</b> that this framework is still under construction. In addition, it is not intended for productive use and written foremost for the developer to improve in PHP.<br>
        Your footer information, e.g. Impressum, Features, Copyright
      </footer>
    </div>


    <script src="<?= PUB_PATH ?>public/assets/js/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="<?= PUB_PATH ?>public/assets/js/bootstrap.min.js"></script>
  </body>
</html> 