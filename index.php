<?php

session_start();

require_once(__DIR__ . "/bootstrap.php");

// Creates the layout
require_once TEMPLATE_DIR . "/header.php";
Minframe\Core\Router::route();
require_once TEMPLATE_DIR . "/footer.php";